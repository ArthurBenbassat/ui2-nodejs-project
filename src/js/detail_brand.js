const brandId = document.getElementById("detail-brand-id");
const name = document.getElementById("detail-brand-name");
const founded = document.getElementById("detail-brand-founded");
const country = document.getElementById("detail-brand-country");
const number_of_employees = document.getElementById("detail-brand-number_of_employees");
const urlLogo = document.getElementById("detail-brand-url-logo");
const logo = document.getElementById("detail-brand-logo");
import { loadBrands } from "./search_brands.js";
export async function getBrand() {
    try {
        const response = await fetch(`http://localhost:3000/brand/${brandId.value}`);
        const data = await response.json();
        name.value = data.name;
        founded.value = data.founded;
        country.value = data.country;
        number_of_employees.value = data.number_of_employees;
		urlLogo.value = data.logo;
        logo.src = data.logo;
        
    } catch (error) {
        console.log(error);
        document.getElementById("detail-error-message").innerHTML = "Error loading brand";
    }
}


export async function changeBrand() {
	const brandError = document.getElementById("detail-error-message");
	try {
		const data = {
			"name": name.value,
			"founded": founded.value,
			"country": country.value,
			"logo": urlLogo.value,
			"number_of_employees": number_of_employees.value
		};
		const response = await fetch(`http://localhost:3000/brand/${brandId.value}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(data)
		});
		if (response.status === 200) {
			brandError.innerHTML = "Successfully changed brand";
			await loadBrands();
		} else {
			throw new Error("Error changing brand");
		}
	} catch (error) {
		console.log(error);
		brandError.innerHTML = "Error loading brands";
	}
}


export async function deleteBrand() {
	const brandError = document.getElementById("error-message");
	try {
		const response = await fetch(`http://localhost:3000/brand/${brandId.value}`, {
			method: "DELETE"
		});
		if (response.status === 200) {
			const divSearch = document.getElementById("about");
			const searchTab = document.getElementById("about-tab");
			divSearch.className = divSearch.className.replace("active", "");
			searchTab.className = searchTab.className.replace("active", "");

			const divAbout = document.getElementById("search");
			const aboutTab = document.getElementById("search-tab");
			divAbout.className = divAbout.className + " active";
			aboutTab.className = aboutTab.className + " active";
			divAbout.style.opacity = 1;
			await loadBrands();
		} else {
			throw new Error("Error deleting brand");
		}
	} catch (error) {
		console.log(error);
		brandError.innerHTML = "Error deleting brand";
	}
}