const home = document.getElementById("card-brands");
home.addEventListener("click", ev => checkOutDetail(ev.target.dataset));
import { getBrand } from "./detail_brand.js";
function checkOutDetail(data) {
    if (data.brandId !== undefined) {
        const divSearch = document.getElementById("home");
		const searchTab = document.getElementById("home-tab");
		divSearch.className = divSearch.className.replace("active", "");
		searchTab.className = searchTab.className.replace("active", "");
	
		//divSearch.style.display = "none";
		document.getElementById("detail-brand-id").value = data.brandId;
	
		const divAbout = document.getElementById("about");
		const aboutTab = document.getElementById("about-tab");
		divAbout.className = divAbout.className + " active";
		aboutTab.className = aboutTab.className + " active";
		divAbout.style.opacity = 1;
		getBrand();
    }
}