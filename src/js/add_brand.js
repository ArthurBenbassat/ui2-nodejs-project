export default async function addBrand() {
	const brandError = document.getElementById("error-message");
	try {
		const name = document.getElementById("brand-name").value;
		const founded = document.getElementById("brand-founded").value;
		const country = document.getElementById("brand-country").value;
		const number_of_employees = document.getElementById("brand-number_of_employees").value;
		const data = {
			"name": name,
			"founded": founded,
			"country": country,
			"number_of_employees": number_of_employees
		};
		const response = await fetch("http://localhost:3000/brand", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(data)
		});
		if (response.status === 201) {
			brandError.innerHTML = "Successfully added brand";
		} else {
			throw new Error("Error adding brand");
		}
	} catch (error) {
		console.log(error);
		brandError.innerHTML = "Error loading brands";
	}
}