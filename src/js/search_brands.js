export const brandInput = document.getElementById("brandInput");
const brandsList = document.getElementById("brands-table-body");
brandsList.addEventListener("click", ev => checkOutDetail(ev.target.dataset))

import { getBrand } from "./detail_brand.js";

class Brand {
	constructor(id, name, founded, country, number_of_employees, logo) {
		this.id = id;
		this.name = name;
		this.founded = founded;
		this.country = country;
		this.number_of_employees = number_of_employees;
		this.logo = logo;
	}
}
export async function loadBrands() {
	try {
		const response = await fetch("http://localhost:3000/brand");
		const data = await response.json();
		
		const brands = data.map(brand => new Brand(brand.id, brand.name, brand.founded, brand.country, brand.number_of_employees, brand.logo));
		const brandsTableBody = document.getElementById("brands-table-body");
		const home = document.getElementById("card-brands");
		brands.forEach(brand => {
			brandsTableBody.insertAdjacentHTML("beforeend", `<tr data-brand-id="${brand.id}">
			<th scope="row" data-brand-id="${brand.id}">${brand.id}</th>
			<td data-brand-id="${brand.id}">${brand.name}</td>
			<td data-brand-id="${brand.id}">${brand.founded}</td>
			<td data-brand-id="${brand.id}">${brand.country}</td>
			<td data-brand-id="${brand.id}">${brand.number_of_employees}</td>
		  </tr>`);
		home.insertAdjacentHTML("beforeend", `<div class="col-sm-6">
		<div class="card" style="height:300px" >
		  <div class="card-body">
			<h5 class="card-title">${brand.name}</h5>
			<img src="${brand.logo}" class="card-img">
			<div data-brand-id="${brand.id}" class="btn btn-primary">Bekijk de detail</div>
		  </div>
		</div>
	  </div>`);
		});
	} catch (error) {
		console.log(error);
		const brandError = document.getElementById("brand-error");
		brandError.innerHTML = "Error loading brands";
	}
}


export function searchBrand() {
	const filter = brandInput.value.toUpperCase();
	const table = document.getElementById("brandTable");
	const tr = table.getElementsByTagName("tr");

	for (let i = 0; i < tr.length; i++) {
		const tdName = tr[i].getElementsByTagName("td")[0];
		const tdCountry = tr[i].getElementsByTagName("td")[2];
		if (tdName || tdCountry) { 
			const txtName = tdName.textContent || tdName.innerText;
			const txtCountry = tdCountry.textContent || tdCountry.innerText;
			if (txtName.toUpperCase().indexOf(filter) > -1 || txtCountry.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

export function checkOutDetail(data) {
	if (data.brandId !== undefined) {
		const divSearch = document.getElementById("search");
		const searchTab = document.getElementById("search-tab");
		divSearch.className = divSearch.className.replace("active", "");
		searchTab.className = searchTab.className.replace("active", "");
	
		//divSearch.style.display = "none";
		document.getElementById("detail-brand-id").value = data.brandId;
	
		const divAbout = document.getElementById("about");
		const aboutTab = document.getElementById("about-tab");
		divAbout.className = divAbout.className + " active";
		aboutTab.className = aboutTab.className + " active";
		divAbout.style.opacity = 1;
		getBrand();
	}
}