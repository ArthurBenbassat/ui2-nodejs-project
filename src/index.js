import "./css/style.scss";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import addBrand from "./js/add_brand.js";
import { loadBrands, searchBrand, brandInput } from "./js/search_brands.js";
import {changeBrand, getBrand, deleteBrand} from "./js/detail_brand.js";
import {} from "./js/home.js";
brandInput.addEventListener("keyup", searchBrand);
const btnAddBrand = document.getElementById("add-brand");
btnAddBrand.addEventListener("click", async () => { addBrand(); });
loadBrands();
getBrand();
const detailChangeBrand = document.getElementById("detail-change-brand");
detailChangeBrand.addEventListener("click", async () => { changeBrand(); });
const detailDeleteBrand = document.getElementById("detail-delete-brand");
detailDeleteBrand.addEventListener("click", async () => { deleteBrand(); });






